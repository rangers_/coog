const TOGGLER = 'TOGGLER';
const SWITCH = 'SWITCH';
const NOTIFHANDLER = 'NOTIFHANDLER';
const UPDATINGDATAPERSONAL = 'UPDATINGDATAPERSONAL';
const COMMUNITIES = {
    "air": {
        name: "Air community",
        image: require('../../assets/images/land.jpg'),
        actions: "200 action",
        activeActions: "200 active action",
        contributor: "5695 contributor",
        selected: false
    },"noise": {
        name: "Noise community",
        image: require('../../assets/images/land.jpg'),
        actions: "200 action",
        activeActions: "200 active action",
        contributor: "5695 contributor",
        selected: false
    },"land": {
        name: "Land community",
        image: require('../../assets/images/land.jpg'),
        actions: "200 action",
        activeActions: "200 active action",
        contributor: "5695 contributor",
        selected: false
    },"light": {
        name: "Light community",
        image: require('../../assets/images/land.jpg'),
        actions: "200 action",
        activeActions: "200 active action",
        contributor: "5695 contributor",
        selected: false
    },"radioactive": {
        name: "Radioactive community",
        image: require('../../assets/images/land.jpg'),
        actions: "200 action",
        activeActions: "200 active action",
        contributor: "5695 contributor",
        selected: false
    }
};

export {
    TOGGLER,
    COMMUNITIES,
    NOTIFHANDLER,
    UPDATINGDATAPERSONAL,
    SWITCH
};