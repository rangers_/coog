import {StyleSheet} from 'react-native';
import {GlobalSheet,Colors} from '../../config';

const actionComponentStyleSheet = StyleSheet.create({
    Image: {
        borderColor: Colors.secondary,
        borderWidth: 2,
    },
    MainV:{
        flexDirection: 'row',
        left: GlobalSheet.widthPercentageToDP(-9)
    },
    description:{
       marginLeft: GlobalSheet.widthPercentageToDP(5),
        opacity:0.9,
        fontSize:GlobalSheet.widthPercentageToDP(5)

    }
});
export default actionComponentStyleSheet;