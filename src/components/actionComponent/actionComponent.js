import React, {Component} from 'react';
import {Text, View, Image, TouchableNativeFeedback} from 'react-native';
import actionComponentStyleSheet from "./actionComponentStyleSheet";
import PropTypes from "prop-types";
import {GlobalSheet} from '../../config';
import Colors from "../../config/colors";

export class ActionComponent extends Component {
    communityName = "Land Community";
    eventName = "Forestation";
    started = "02/12/2018";
    contributions = "123";
    location = "Monastir";
    image;

    constructor(props) {
        super(props);
        if (this.props.height) {
            this.imageStyle = {
                ...actionComponentStyleSheet.Image,
                height: this.props.height,
                width: this.props.height, //shut up webstorm I know what to do!
                borderRadius: this.props.height / 2
            }
        } else {
            this.imageStyle = {
                ...actionComponentStyleSheet.Image,
                height: GlobalSheet.widthPercentageToDP(30),
                width: GlobalSheet.widthPercentageToDP(30),
                borderRadius: GlobalSheet.widthPercentageToDP(30) / 2
            };
        }

        if (this.props.image) this.image = this.props.image;
        else this.image = require('../../../assets/land.jpg');

    }

    render() {

        return (
            <View style={actionComponentStyleSheet.MainV}>
                <Image source={this.image} style={this.imageStyle}></Image>
                <View style={actionComponentStyleSheet.description}>
                    <Text style={{
                        fontWeight: 'bold',
                        color: 'black',
                        fontSize: GlobalSheet.widthPercentageToDP(7)
                    }}>{this.props.eventName ? this.props.eventName : this.eventName}</Text>
                    <Text><Text
                        style={{fontWeight: 'bold', color: 'black', fontSize: GlobalSheet.widthPercentageToDP(5)}}>Started
                        : </Text><Text style={{
                        color: 'black',
                        opacity: 0.7
                    }}>{this.props.started ? this.props.started : this.started}</Text></Text>
                    <Text><Text
                        style={{fontWeight: 'bold', color: 'black', fontSize: GlobalSheet.widthPercentageToDP(5)}}>Contributors
                        : </Text><Text style={{
                        color: 'black',
                        opacity: 0.7
                    }}>{this.props.contributions ? this.props.contributions : this.contributions}</Text></Text>
                    <Text><Text
                        style={{fontWeight: 'bold', color: 'black', fontSize: GlobalSheet.widthPercentageToDP(5)}}>Location
                        : </Text><Text style={{
                        color: 'black',
                        opacity: 0.7
                    }}>{this.props.location ? this.props.location : this.location}</Text></Text>
                </View>
            </View>
        );
    }
}