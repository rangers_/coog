import {CommunitySlide} from './communitySlideComponent';
import {ActionComponent} from "./actionComponent";

export {
    CommunitySlide,
    ActionComponent
}