import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

const communitySlideStyleSheet = StyleSheet.create({
    slideView: {
        width: GlobalSheet.widthPercentageToDP(85),
        height: GlobalSheet.heightPercentageToDP(70),
        borderRadius: 20,
        elevation: 5,
        color: "#fff"
    },
    imageStyle: {
        width: GlobalSheet.widthPercentageToDP(40),
        height: GlobalSheet.widthPercentageToDP(40),
        borderRadius: (GlobalSheet.widthPercentageToDP(40) / 2),
        borderWidth: 3,
        borderColor: Colors.secondary
    },
    imageContainer: {
        width: GlobalSheet.widthPercentageToDP(100),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        paddingTop: GlobalSheet.heightPercentageToDP(7)
    },
    nameContainer: {
        width: GlobalSheet.widthPercentageToDP(100),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        paddingTop: GlobalSheet.heightPercentageToDP(3)
    },
    nameStyle: {
        fontSize: GlobalSheet.widthPercentageToDP(9),
        fontWeight: 'bold',
        paddingLeft: GlobalSheet.widthPercentageToDP(10),
        paddingRight: GlobalSheet.widthPercentageToDP(10),
        textAlign: 'center'
    },
    dataContainer: {
        width: GlobalSheet.widthPercentageToDP(100),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        paddingTop: GlobalSheet.heightPercentageToDP(7)
    },
    dataStyle: {
        fontSize: GlobalSheet.widthPercentageToDP(8),
        fontWeight: 'bold',
        opacity: 0.8
    }
});

export {
    communitySlideStyleSheet
}