import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';
import {communitySlideStyleSheet} from './communitySlideStyleSheet';

export default class CommunitySlide extends Component {

    imageDummy = require('../../../assets/images/land.jpg');
    nameDummy = "Land community";
    actionsDummy = "152 actions";
    activeActionsDummy = "45 active actions";
    contributorsDummy = "456 contributor";

    render() {
        return (
            <View style={communitySlideStyleSheet.slideView}>
                <View style={communitySlideStyleSheet.imageContainer}>
                    <Image source={this.props.image ? this.props.image : this.imageDummy}
                           style={communitySlideStyleSheet.imageStyle}/>
                </View>
                <View style={communitySlideStyleSheet.nameContainer}>
                    <Text style={communitySlideStyleSheet.nameStyle}>{this.props.name ? this.props.name : this.nameDummy}</Text>
                </View>
                <View style={communitySlideStyleSheet.dataContainer}>
                    <Text style={communitySlideStyleSheet.dataStyle}>{this.props.contributors ? this.props.contributors : this.contributorsDummy}</Text>
                    <Text style={communitySlideStyleSheet.dataStyle}>{this.props.actions ? this.props.actions : this.actionsDummy}</Text>
                    <Text style={communitySlideStyleSheet.dataStyle}>{this.props.activeActions ? this.props.activeActions : this.activeActionsDummy}</Text>
                </View>
            </View>
        )
    }
}