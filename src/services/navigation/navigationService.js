import { NavigationActions } from 'react-navigation';

const navigators = {
    tabs: {name: 'tabsNavigator'},
    daddy: {name: 'daddyNavigator'}
};
let _navigator = {};
let _navParams = {};

function setNavigator(navigatorRef, name: string) {
    _navigator[name] = navigatorRef;
}

function setNavParams(navigatorName, params) {
    _navParams[navigatorName] = params;
}

function navigate(navigatorName, routeName, params) {
    setNavParams(navigatorName, params);
    console.log(_navParams[navigatorName]);
    _navigator[navigatorName].dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    );
}

function getNavParams(navigatorName) {
    console.log(_navParams[navigatorName]);
    return _navParams[navigatorName];
}

export default {
    navigate,
    setNavigator,
    navigators,
    getNavParams,
    setNavParams
};