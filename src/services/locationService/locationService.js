const geoCodeLatLngURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
const geoCodeAddressURL = "https://maps.googleapis.com/maps/api/geocode/json?address=";
const key = "&key=AIzaSyCFYxF9-Bgz7_d5TfKlRs6rPttMfINk_bI";

function askGoogleLatLng(lat, lng) {
    return fetch(geoCodeLatLngURL + lat + ',' + lng + key)
}
function askGoogleAddress(address) {
    return fetch(geoCodeAddressURL + address + key)
}

export {
    askGoogleAddress,
    askGoogleLatLng
}