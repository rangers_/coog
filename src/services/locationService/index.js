import {askGoogleLatLng, askGoogleAddress} from './locationService';

export {
    askGoogleAddress,
    askGoogleLatLng
}