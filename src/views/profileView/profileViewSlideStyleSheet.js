import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

const profileViewSlideStyleSheet = StyleSheet.create({
    sliderView: {
        flex: 1
    }
});

export {
    profileViewSlideStyleSheet
}