import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';
import {actionsViewSlideStyleSheet} from './actionsViewSlideStyleSheet';
import {COMMUNITIES} from '../../config';
import SwipeCards from 'react-native-swipe-cards';
import {CommunitySlide} from "../../components";

class NoMoreCards extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Text style={{
                    fontSize: 22
                }}>No more cards</Text>
            </View>
        )
    }
}

export default class ActionsView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            commsArray: Object.keys(COMMUNITIES)
        }
    }

    render() {
        return (
            <View style={actionsViewSlideStyleSheet.sliderView}>
                <SwipeCards
                    cards={this.state.commsArray}
                    renderCard={(cardData) => <CommunitySlide name={COMMUNITIES[cardData].name} actions={COMMUNITIES[cardData].actions}
                                                              image={COMMUNITIES[cardData].imageStyle}
                                                              activeActions={COMMUNITIES[cardData].activeActions}
                                                              contributor={COMMUNITIES[cardData].contributor}/>}
                    renderNoMoreCards={() => <NoMoreCards/>}
                    loop={true}
                    enablePrev={true}
                    showYup={false}
                    showMaybe={false}
                    showNope={false}
                />
            </View>
        )
    }
}