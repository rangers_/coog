import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

const actionsViewSlideStyleSheet = StyleSheet.create({
    sliderView: {
        flex: 1
    }
});

export {
    actionsViewSlideStyleSheet
}