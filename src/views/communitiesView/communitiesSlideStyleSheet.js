import {StyleSheet} from 'react-native';
import {GlobalSheet, Colors} from '../../config';

const communitiesSlideStyleSheet = StyleSheet.create({
    sliderView: {
        flex: 1
    }
});

export {
    communitiesSlideStyleSheet
}