import React, {Component} from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import {NavigationService} from '../../services';
import {TabsRouter} from '../index';

const MainDaddy = createStackNavigator(
    {
        mainTabs: TabsRouter
    }
);

const AppRoutingContainer = createAppContainer(MainDaddy);

export default class DaddyRouter extends Component {
    render() {
        return (
            <AppRoutingContainer ref={navRef => NavigationService.setNavigator(navRef, NavigationService.navigators.daddy.name)}/>
        );
    }
}