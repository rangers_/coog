import React, {Component} from 'react';
import {createMaterialTopTabNavigator, createAppContainer} from 'react-navigation';
import {NavigationService} from '../../services';

const MainTabs = createMaterialTopTabNavigator(
    {}
);

const TabsAppRouter = createAppContainer(MainTabs);

export default class TabsRouter extends Component {
    render() {
        return (
            <TabsAppRouter ref={navRef => NavigationService.setNavigator(navRef, NavigationService.navigators.tabs.name)}/>
        );
    }
}