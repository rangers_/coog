import {TabsRouter} from './tabsRouter';
import {DaddyRouter} from './daddyRouter';

export {
    DaddyRouter,
    TabsRouter
}